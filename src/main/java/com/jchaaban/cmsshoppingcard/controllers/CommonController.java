package com.jchaaban.cmsshoppingcard.controllers;

import com.jchaaban.cmsshoppingcard.models.PageRepository;
import com.jchaaban.cmsshoppingcard.models.data.Category;
import com.jchaaban.cmsshoppingcard.models.data.Page;
import com.jchaaban.cmsshoppingcard.services.CardService;
import com.jchaaban.cmsshoppingcard.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.List;

@ControllerAdvice
public class CommonController {

    private final PageRepository pageRepository;

    private final CategoryService categoryService;

    private final CardService cardService;

    public CommonController(
            PageRepository pageRepository,
            CategoryService categoryService,
            CardService cardService
    ) {
        this.pageRepository = pageRepository;
        this.categoryService = categoryService;
        this.cardService = cardService;
    }

    @ModelAttribute
    public void sharedData(HttpSession session, Model model, Principal principal) {

        if (principal != null)
            model.addAttribute("principal", principal.getName());

        List<Category> categories = categoryService.findAllByOrderBySortingAsc();
        List<Page> pages = pageRepository.findAllByOrderBySortingAsc();

        if (session.getAttribute("card") != null){
            cardService.updateCardStatus(session,model);
        }

        model.addAttribute("commonPages", pages);
        model.addAttribute("commonCategories",categories);
    }
}
