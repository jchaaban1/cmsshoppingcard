INSERT INTO address (id, city, street, zip_code) VALUES (1, 'Test-city', 'Test-Street 1', '12345');
INSERT INTO address (id, city, street, zip_code) VALUES (2, 'Test-city', 'Test-Street 2', '12345');

INSERT INTO users (id, email, is_admin, is_enabled, password, phone_number, username, address_id) VALUES (1, 'adminn@outlook.com', true, true, '$2a$10$lOCxdB0mx..Td5Kb2S5eX.aFUjoO/bi75e4PopERj7bA9B1hkA2.e', '123-456-7891', 'admin', 1);
INSERT INTO users (id, email, is_admin, is_enabled, password, phone_number, username, address_id) VALUES (2, 'jaafar13chaaban@outlook.com', false, true, '$2a$10$lOCxdB0mx..Td5Kb2S5eX.aFUjoO/bi75e4PopERj7bA9B1hkA2.e', '123-456-7891', 'jaafar', 2);

INSERT INTO pages VALUES (1, '<h1>Welcome</h1>', 'home', 100, 'Home');

INSERT INTO categories VALUES (1, 'Computers', 'computers', 100);

SELECT setval('address_id_seq', (SELECT MAX(id) FROM address));
SELECT setval('users_id_seq', (SELECT MAX(id) FROM users));
SELECT setval('pages_id_seq', (SELECT MAX(id) FROM pages));
SELECT setval('categories_id_seq', (SELECT MAX(id) FROM categories));
