FROM adoptopenjdk:11-jdk-hotspot

VOLUME /tmp

EXPOSE 5050

ARG JAR_FILE=target/*.jar

ADD ${JAR_FILE} app.jar

CMD ["java","-Dspring.profiles.active=docker", "-jar","/app.jar"]
